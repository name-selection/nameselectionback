FROM openjdk:11-jdk-slim
COPY build/libs/name_selection-0.0.1-SNAPSHOT.jar .
EXPOSE 8080
CMD ["java", "-jar", "name_selection-0.0.1-SNAPSHOT.jar"]