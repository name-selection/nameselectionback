# Name Selection Server

Серверная часть проекта Name Selection


##Компиляция докер образа и запуск docker-compose

- Сбилдить приложение gradle -> build
- Сбилдить docker image: docker build -t название_образа .
- Проверить в папке docker-compose.yml поле services -> back -> image. Поле должно содержать название_образа нашего docker image
- Запустить docker-compose up

После завершения работы вызвать docker-compose down


##Основные команды Docker

- билд образа: docker build -t название_образа .
- запуск сервера: docker-compose up
- остановка сервера: docker-compose down
- удалить все контейнеры: docker rm $(docker ps -aq)
- удалить контейнер: docker rm \[CONTAINER_ID or CONTAINER_NAME:TAG\]
- удалить все образы: docker rmi $(docker images -aq)
- удалить образ: docker rmi \[IMAGE_ID or IMAGE_NAME:TAG\]
