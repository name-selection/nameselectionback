package ru.team46.name_selection;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import ru.team46.name_selection.entities.name.Name;
import ru.team46.name_selection.repositories.name.NameRepository;
import ru.team46.name_selection.services.name.NameService;

import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import static org.mockito.ArgumentMatchers.any;

@SpringBootTest
public class NameServiceTests {
    @Mock
    private NameRepository mockNameRepository;

    private NameService nameServiceUnderTest;

    @Before
    public void setup() {
        MockitoAnnotations.openMocks(this);
        nameServiceUnderTest = new NameService(mockNameRepository);

        Name testName = new Name();
        testName.setName("test");

        Mockito.when(mockNameRepository.save(any()))
                .thenReturn(testName);
    }

    @Test
    public void testCreateName() {
        Name name = new Name();
        name.setName("test");

        Optional<Name> result = nameServiceUnderTest.save(name);
        // Verify the results
        assertEquals(name.getName(), result.get().getName());
    }

}
