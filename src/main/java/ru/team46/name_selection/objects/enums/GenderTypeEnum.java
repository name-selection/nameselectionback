package ru.team46.name_selection.objects.enums;

public enum GenderTypeEnum {
    MALE, FEMALE, ANY
}
