package ru.team46.name_selection.objects.enums;

public enum ErrorType {
    ENTITY_WAS_NOT_SAVED,
    ENTITY_WAS_NOT_UPDATED,
    ENTITY_WAS_NOT_FOUND,
    ENTITY_WAN_NOT_DELETED
}
