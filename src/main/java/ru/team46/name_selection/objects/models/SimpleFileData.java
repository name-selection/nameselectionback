package ru.team46.name_selection.objects.models;

import java.util.Date;
import java.util.UUID;

public class SimpleFileData {
    private UUID uuid;
    private String name;
    private String type;
    private Long size;
    private Date uploadDate;

    public SimpleFileData(UUID uuid, String name, String type, Long size, Date uploadDate) {
        this.uuid = uuid;
        this.name = name;
        this.type = type;
        this.size = size;
        this.uploadDate = uploadDate;
    }

    public UUID getUuid() {
        return uuid;
    }

    public void setUuid(UUID uuid) {
        this.uuid = uuid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Long getSize() {
        return size;
    }

    public void setSize(Long size) {
        this.size = size;
    }

    public Date getUploadDate() {
        return uploadDate;
    }

    public void setUploadDate(Date uploadDate) {
        this.uploadDate = uploadDate;
    }
}
