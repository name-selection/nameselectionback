package ru.team46.name_selection.objects.models;

import ru.team46.name_selection.objects.enums.GenderTypeEnum;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;

public class NameDto {
    private String name;
    private String description;
    private String nameDays;
    private String nameVariants;
    @Enumerated(EnumType.STRING)
    private GenderTypeEnum gender;

    public NameDto() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getNameDays() {
        return nameDays;
    }

    public void setNameDays(String nameDays) {
        this.nameDays = nameDays;
    }

    public String getNameVariants() {
        return nameVariants;
    }

    public void setNameVariants(String nameVariants) {
        this.nameVariants = nameVariants;
    }

    public GenderTypeEnum getGender() {
        return gender;
    }

    public void setGender(GenderTypeEnum gender) {
        this.gender = gender;
    }
}
