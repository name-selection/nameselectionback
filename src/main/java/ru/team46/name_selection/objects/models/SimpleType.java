package ru.team46.name_selection.objects.models;

import java.util.UUID;

public class SimpleType {
    private UUID uuid;
    private String Name;

    public SimpleType(UUID uuid, String name) {
        this.uuid = uuid;
        Name = name;
    }

    public UUID getUuid() {
        return uuid;
    }

    public String getName() {
        return Name;
    }
}
