package ru.team46.name_selection.controllers.file;

import com.google.common.collect.Iterables;
import io.swagger.annotations.ApiOperation;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import ru.team46.name_selection.controllers.common.AbstractController;
import ru.team46.name_selection.entities.file.File;
import ru.team46.name_selection.objects.models.SimpleFileData;
import ru.team46.name_selection.services.file.FileService;

import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/files")
public class FileController extends AbstractController<File, FileService> {
    public FileController(FileService service) {
        super(service);
    }

    @PostMapping("/upload")
    public ResponseEntity<?> upload(@RequestParam("file") MultipartFile file) {
        if (Objects.equals(file.getContentType(), "application/vnd.android.package-archive")) {
            try {
                File dbFile = new File();
                dbFile.setName(file.getOriginalFilename());
                dbFile.setType(file.getContentType());
                dbFile.setBytes(file.getBytes());
                dbFile.setSize(file.getSize());
                Optional<File> optionalFile = service.save(dbFile);

                if (optionalFile.isPresent()) {
                    File addedFile = optionalFile.get();
                    SimpleFileData simpleFileData = new SimpleFileData(
                            addedFile.getUuid(),
                            addedFile.getName(),
                            addedFile.getType(),
                            addedFile.getSize(),
                            addedFile.getUploadDate()
                    );
                    return ResponseEntity.ok(simpleFileData);
                } else {
                    return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
                }

            } catch (IOException e) {
                e.printStackTrace();
                return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(e.getMessage());
            }
        } else {
            Optional<String> message = Optional.of("Неверный тип файла. " +
                    "Ожидался application/vnd.android.package-archive " +
                    "Получен " + file.getContentType());
            return ResponseEntity.status(HttpStatus.I_AM_A_TEAPOT).body(message);
        }
    }

    @GetMapping("/download")
    public ResponseEntity<?> download() {
        Optional<List<File>> optionalFiles = service.findAll();
        if (optionalFiles.isPresent()) {
            List<File> files = optionalFiles.get();
            File lastFile = Iterables.getLast(files);

            HttpHeaders headers = new HttpHeaders();
            headers.add(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=" + lastFile.getName());
            headers.add("Cache-Control", "no-cache, no-store, must-revalidate");
            headers.add("Pragma", "no-cache");
            headers.add("Expires", "0");

            return ResponseEntity.ok()
                    .headers(headers)
                    .contentLength(lastFile.getBytes().length)
                    .contentType(MediaType.MULTIPART_FORM_DATA)
                    .body(lastFile.getBytes());
        }
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Файл не найден");
    }

    @GetMapping("/all")
    public ResponseEntity<?> all() {
        Optional<List<File>> optionalFiles = service.findAll();
        if (optionalFiles.isPresent()) {
            List<SimpleFileData> fileDataList = optionalFiles.get().stream().map(file -> new SimpleFileData(
                    file.getUuid(),
                    file.getName(),
                    file.getType(),
                    file.getSize(),
                    file.getUploadDate()
            )).collect(Collectors.toList());
            return ResponseEntity.ok(fileDataList);
        }
        return ResponseEntity.ok(new ArrayList<SimpleFileData>());
    }

    @Override
    @ApiOperation(value = "hidden", hidden = true)
    public ResponseEntity<File> save(File entity) {
        return super.save(entity);
    }

    @Override
    @ApiOperation(value = "hidden", hidden = true)
    public ResponseEntity<File> update(File entity) {
        return super.update(entity);
    }

    @ApiOperation(value = "Find All Files", hidden = true)
    @Override
    public ResponseEntity<List<File>> findAll() {
        return super.findAll();
    }

    @ApiOperation(value = "hidden", hidden = true)
    @Override
    public ResponseEntity<File> findByUUID(UUID uuid) {
        return super.findByUUID(uuid);
    }

    @ApiOperation(value = "hidden", hidden = true)
    @Override
    public ResponseEntity<File> delete(UUID uuid) {
        return super.delete(uuid);
    }

    @PostMapping("/delete")
    public ResponseEntity<?> delete(@RequestParam("uuid") UUID uuid, @RequestParam("apiKey") String apiKey) {
        if (apiKey.equals("1234")) {
            final Optional<File> optionalFile = service.delete(uuid);
            if (optionalFile.isPresent()) {
                File file = optionalFile.get();
                return ResponseEntity.ok(new SimpleFileData(
                        file.getUuid(),
                        file.getName(),
                        file.getType(),
                        file.getSize(),
                        file.getUploadDate()
                ));
            }
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
        return ResponseEntity.status(HttpStatus.FORBIDDEN).build();
    }
}