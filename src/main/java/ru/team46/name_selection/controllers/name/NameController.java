package ru.team46.name_selection.controllers.name;

import com.google.common.collect.Lists;
import io.swagger.annotations.ApiOperation;
import org.springframework.expression.ExpressionException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.team46.name_selection.controllers.common.AbstractController;
import ru.team46.name_selection.entities.name.Name;
import ru.team46.name_selection.objects.enums.ErrorType;
import ru.team46.name_selection.objects.enums.GenderTypeEnum;
import ru.team46.name_selection.objects.enums.SortTypeEnum;
import ru.team46.name_selection.objects.models.NameDto;
import ru.team46.name_selection.objects.models.SimpleType;
import ru.team46.name_selection.services.common.Rating;
import ru.team46.name_selection.services.name.NameService;
import springfox.documentation.annotations.ApiIgnore;

import javax.validation.Valid;
import java.util.*;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/names")
public class NameController extends AbstractController<Name, NameService> {
    public NameController(NameService service) {
        super(service);
    }

    @ApiIgnore
    @ApiOperation(value = "Получение имён")
    @Override
    public ResponseEntity<List<Name>> findAll() {
        return super.findAll();
    }

    @ApiOperation(value = "Получение имён")
    @GetMapping("/find_all")
    public ResponseEntity<?> findAll(@RequestParam(required = false) SortTypeEnum sort) {
        try {
            Optional<List<Name>> optionalNameList = service.findAll();
            if (optionalNameList.isPresent()) {

                final List<Name> nameList = optionalNameList.get();
                if (sort == SortTypeEnum.ASC) {
                    nameList.sort(Comparator.comparing(Name::getName));
                } else {
                    nameList.sort(Comparator.comparing(Name::getName).reversed());
                }
                return ResponseEntity.status(HttpStatus.OK).body(nameList);
            } else {
                return ResponseEntity.status(HttpStatus.NO_CONTENT).body(new ArrayList<Name>());
            }
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(e.getMessage());
        }
    }

    @ApiOperation(value = "Получение имён по пулярности с сортировкой")
    @GetMapping("/popular")
    public ResponseEntity<?> popularNames(@RequestParam(required = false) SortTypeEnum sort) {
        try {
            final Optional<List<Name>> optionalNameList = service.findAll();
            if (optionalNameList.isPresent()) {
                final List<Name> nameList = optionalNameList.get();
                nameList.sort(Comparator.comparingInt(
                        Name::getViews
                ));
                if (sort == SortTypeEnum.ASC) {
                    return ResponseEntity.status(HttpStatus.OK).body(nameList);
                } else {
                    return ResponseEntity.status(HttpStatus.OK).body(Lists.reverse(nameList));
                }
            } else {
                return ResponseEntity.status(HttpStatus.NO_CONTENT).body(new ArrayList<Name>());
            }
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(e.getMessage());
        }
    }

    @ApiIgnore
    @ApiOperation(value = "Получение имени по UUID")
    @Override
    public ResponseEntity<Name> findByUUID(UUID uuid) {
        return super.findByUUID(uuid);
    }

    @ApiOperation(value = "Получение имени по UUID")
    @GetMapping(value = "/find_name")
    public ResponseEntity<?> find_name(@RequestParam UUID uuid) {
        Optional<Name> optionalName = service.findByUUID(uuid);
        if (optionalName.isPresent()) {
            final Name name = optionalName.get();
            name.setViews(name.getViews() + 1);
            final Optional<Name> updatedName = service.update(name);
            return ResponseEntity.status(HttpStatus.OK).body(updatedName);
        }
        return ResponseEntity.status(HttpStatus.NO_CONTENT).body(new ArrayList<>());
    }

    @ApiOperation(value = "Сохранить множеством имён")
    @PostMapping("/saveMany")
    public ResponseEntity<?> saveMany(@RequestBody @Valid List<NameDto> names) {
        List<Name> nameList = new ArrayList<>();
        try {
            for (NameDto name : names) {
                Name entity = new Name();
                entity.setName(name.getName());
                entity.setDescription(name.getDescription());
                entity.setNameDays(name.getNameDays());
                entity.setNameVariants(name.getNameVariants());
                entity.setGender(name.getGender());
                entity.setViews(0);

                final Optional<Name> optionalName = service.save(entity);
                optionalName.ifPresentOrElse(nameList::add,
                        () -> {
                            throw new ExpressionException(ErrorType.ENTITY_WAS_NOT_SAVED.name());
                        }
                );
            }
        } catch (Exception e) {
            return ResponseEntity.badRequest().build();
        }
        return ResponseEntity.ok(nameList);
    }

    @ApiOperation(value = "Сохранить имя")
    @PostMapping("/saveName")
    public ResponseEntity<?> saveName(@RequestBody @Valid NameDto name) {
        Name entity = new Name();
        entity.setName(name.getName());
        entity.setDescription(name.getDescription());
        entity.setNameDays(name.getNameDays());
        entity.setNameVariants(name.getNameVariants());
        entity.setGender(name.getGender());
        entity.setViews(0);

        Optional<Name> response = service.save(entity);
        if (response.isPresent()) {
            return ResponseEntity.status(HttpStatus.OK).body(response.get());
        }
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Ошибка при сохранении");
    }

    @ApiIgnore
    @ApiOperation(value = "Устаревший метод, вметсо него использовать /saveName")
    @PostMapping("/save")
    @Override
    public ResponseEntity<Name> save(Name entity) {
        return service.save(entity).map(ResponseEntity::ok)
                .orElseThrow(() -> new ExpressionException(ErrorType.ENTITY_WAS_NOT_SAVED.name()));
    }

    @ApiOperation(value = "Получение случайного имени")
    @GetMapping("/random")
    ResponseEntity<?> randomName(@RequestParam GenderTypeEnum gender) {
        final Optional<Name> optionalName = service.random(gender);
        if (optionalName.isPresent()) {
            final Name name = optionalName.get();
            name.setViews(name.getViews() + 1);
            final Optional<Name> updatedName = service.update(name);
            return ResponseEntity.status(HttpStatus.OK).body(updatedName);
        }
        return optionalName.map(e -> new ResponseEntity<>(e, HttpStatus.OK))
                .orElseGet(() -> ResponseEntity.noContent().build());
    }

    @ApiOperation(value = "Получение простых мужских имён")
    @GetMapping("/males")
    ResponseEntity<List<Name>> maleNames() {
        final Optional<List<Name>> optionalNameList = service.findAll();
        return optionalNameList.map(names ->
                new ResponseEntity<>(names.stream().filter(name -> name.getGender() == GenderTypeEnum.MALE)
                        .collect(Collectors.toList()), HttpStatus.OK))
                .orElseGet(() -> ResponseEntity.noContent().build());
    }

    @ApiOperation(value = "Получение простых женских имён")
    @GetMapping("/females")
    ResponseEntity<List<Name>> femaleNames() {
        final Optional<List<Name>> optionalNameList = service.findAll();
        return optionalNameList.map(names ->
                new ResponseEntity<>(names.stream().filter(name -> name.getGender() == GenderTypeEnum.FEMALE)
                        .collect(Collectors.toList()), HttpStatus.OK))
                .orElseGet(() -> ResponseEntity.noContent().build());
    }

    @ApiOperation(value = "Получение простых моделей мужских имён")
    @GetMapping("/simple_males")
    ResponseEntity<List<SimpleType>> maleNamesSimple() {
        final Optional<List<Name>> optionalNameList = service.findAll();
        return optionalNameList.map(names ->
                new ResponseEntity<>(names.stream().filter(name -> name.getGender() == GenderTypeEnum.MALE)
                        .map(name -> new SimpleType(name.getUuid(), name.getName()))
                        .collect(Collectors.toList()), HttpStatus.OK))
                .orElseGet(() -> ResponseEntity.noContent().build());
    }

    @ApiOperation(value = "Получение простых моделей женских имён")
    @GetMapping("/simple_females")
    ResponseEntity<List<SimpleType>> femaleNamesSimple() {
        final Optional<List<Name>> optionalNameList = service.findAll();
        return optionalNameList.map(names ->
                new ResponseEntity<>(names.stream().filter(name -> name.getGender() == GenderTypeEnum.FEMALE)
                        .map(name -> new SimpleType(name.getUuid(), name.getName()))
                        .collect(Collectors.toList()), HttpStatus.OK))
                .orElseGet(() -> ResponseEntity.noContent().build());
    }

    @ApiOperation(value = "Совместимость имён")
    @PostMapping("/compatibility")
    ResponseEntity<String> namesCompatibility(@RequestParam UUID firstNameUuid, @RequestParam UUID secondNameUuid) {
        final Optional<Name> optionalFirstName = service.findByUUID(firstNameUuid);
        final Optional<Name> optionalSecondName = service.findByUUID(secondNameUuid);

        if (optionalFirstName.isEmpty()) {
            return ResponseEntity.of(Optional.of("Name with firstNameUuid wasn't found!"));
        }
        if (optionalSecondName.isEmpty()) {
            return ResponseEntity.of(Optional.of("Name with firstNameUuid wasn't found!"));
        }

        final String firstName = optionalFirstName.get().getName();
        final String secondName = optionalSecondName.get().getName();

        return ResponseEntity.ok(Rating.getNameCompatibilityDescription(firstName, secondName));
    }
}
