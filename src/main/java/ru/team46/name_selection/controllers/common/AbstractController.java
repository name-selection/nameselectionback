package ru.team46.name_selection.controllers.common;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.expression.ExpressionException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import ru.team46.name_selection.entities.common.AbstractEntity;
import ru.team46.name_selection.services.common.ICommonService;
import ru.team46.name_selection.objects.enums.ErrorType;


import java.util.List;
import java.util.Optional;
import java.util.UUID;


public class AbstractController<E extends AbstractEntity,
        S extends ICommonService<E>> implements ICommonController<E> {

    protected final S service;

    @Autowired
    public AbstractController(S service) {
        this.service = service;
    }

    @Override
    public ResponseEntity<E> save(E entity) {
        return service.save(entity).map(ResponseEntity::ok)
                .orElseThrow(() -> new ExpressionException(ErrorType.ENTITY_WAS_NOT_SAVED.name()));
    }

    @Override
    public ResponseEntity<E> update(E entity) {
        return service.update(entity).map(ResponseEntity::ok)
                .orElseThrow(() -> new ExpressionException(ErrorType.ENTITY_WAS_NOT_UPDATED.name()));
    }

    @Override
    public ResponseEntity<List<E>> findAll() {
        final Optional<List<E>> optionalES = service.findAll();
        return optionalES.map(e -> new ResponseEntity<>(e, HttpStatus.OK))
                .orElseGet(() -> ResponseEntity.status(HttpStatus.NO_CONTENT).build());
    }

    @Override
    public ResponseEntity<E> findByUUID(UUID uuid) {
        final Optional<E> optionalE = service.findByUUID(uuid);
        return optionalE.map(e -> new ResponseEntity<>(e, HttpStatus.OK))
                .orElseGet(() -> ResponseEntity.noContent().build());
    }

    @Override
    public ResponseEntity<E> delete(UUID uuid) {
        return service.delete(uuid).map(ResponseEntity::ok)
                .orElseThrow(() -> new ExpressionException(ErrorType.ENTITY_WAN_NOT_DELETED.name()));
    }
}
