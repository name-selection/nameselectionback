package ru.team46.name_selection.controllers.common;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.team46.name_selection.entities.common.AbstractEntity;

import java.util.List;
import java.util.UUID;

public interface ICommonController<E extends AbstractEntity> {

    @PostMapping("/save")
    ResponseEntity<E> save(@RequestBody E entity);

    @PutMapping("/update")
    ResponseEntity<E> update(@RequestBody E entity);

    @GetMapping("/findAll")
    ResponseEntity<List<E>> findAll();

    @GetMapping("/find")
    ResponseEntity<E> findByUUID(@RequestParam UUID uuid);

    @PutMapping("/delete")
    ResponseEntity<E> delete(@RequestParam UUID uuid);
}
