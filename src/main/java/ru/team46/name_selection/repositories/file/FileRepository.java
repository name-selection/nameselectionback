package ru.team46.name_selection.repositories.file;

import org.springframework.stereotype.Repository;
import ru.team46.name_selection.entities.file.File;
import ru.team46.name_selection.repositories.common.ICommonRepository;

@Repository
public interface FileRepository extends ICommonRepository<File> {
}
