package ru.team46.name_selection.repositories.common;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.NoRepositoryBean;
import ru.team46.name_selection.entities.common.AbstractEntity;

import java.util.UUID;

@NoRepositoryBean
public interface ICommonRepository<E extends AbstractEntity> extends JpaRepository<E, UUID> {
}
