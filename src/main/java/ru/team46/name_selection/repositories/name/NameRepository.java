package ru.team46.name_selection.repositories.name;

import org.springframework.stereotype.Repository;
import ru.team46.name_selection.entities.name.Name;
import ru.team46.name_selection.repositories.common.ICommonRepository;

@Repository
public interface NameRepository extends ICommonRepository<Name> {
}
