package ru.team46.name_selection.entities.file;

import ru.team46.name_selection.entities.common.AbstractEntity;

import javax.persistence.Entity;
import javax.persistence.Lob;
import javax.persistence.Table;
import java.util.Date;

@Entity
@Table(name = "Files")
public class File extends AbstractEntity {
    private String name;

    private String type;

    private Long size;

    @Lob
    private byte[] bytes;

    private Date uploadDate;

    public File() {
        this.uploadDate = new Date();
    }

    public File(String name, String type, Long size, byte[] bytes) {
        this.name = name;
        this.type = type;
        this.bytes = bytes;
        this.size = size;
        this.uploadDate = new Date();
    }

    public byte[] getBytes() {
        return bytes;
    }

    public void setBytes(byte[] bytes) {
        this.bytes = bytes;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Date getUploadDate() {
        return uploadDate;
    }

    public Long getSize() {
        return size;
    }

    public void setSize(Long size) {
        this.size = size;
    }
}
