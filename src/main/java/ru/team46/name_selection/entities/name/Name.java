package ru.team46.name_selection.entities.name;

import ru.team46.name_selection.entities.common.AbstractEntity;
import ru.team46.name_selection.objects.enums.GenderTypeEnum;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Table;
import javax.validation.constraints.Size;

@Entity
@Table(name = "Names")
public class Name extends AbstractEntity {
    private String name;

    @Size(max = 4000)
    private String description;

    @Size(max = 2000)
    private String nameDays;

    @Size(max = 2000)
    private String nameVariants;

    @Enumerated(EnumType.STRING)
    private GenderTypeEnum gender;

    private int views = 0;

    public Name() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public GenderTypeEnum getGender() {
        return gender;
    }

    public void setGender(GenderTypeEnum gender) {
        this.gender = gender;
    }

    public String getNameDays() {
        return nameDays;
    }

    public void setNameDays(String nameDays) {
        this.nameDays = nameDays;
    }

    public String getNameVariants() {
        return nameVariants;
    }

    public void setNameVariants(String nameVariants) {
        this.nameVariants = nameVariants;
    }

    public int getViews() {
        return views;
    }

    public void setViews(int views) {
        this.views = views;
    }
}
