package ru.team46.name_selection.services.name;

import org.springframework.stereotype.Service;
import ru.team46.name_selection.entities.name.Name;
import ru.team46.name_selection.objects.enums.GenderTypeEnum;
import ru.team46.name_selection.repositories.name.NameRepository;
import ru.team46.name_selection.services.common.AbstractService;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Random;
import java.util.stream.Collectors;

@Service
public class NameService extends AbstractService<Name, NameRepository> {

    public NameService(NameRepository repository) {
        super(repository);
    }

    public Optional<Name> random(GenderTypeEnum genderVariantsEnum) {
        final Optional<List<Name>> namesResponse = this.findAll();
        if (namesResponse.isPresent()) {
            final List<Name> namesOfResponse = namesResponse.get();
            List<Name> names;
            Random rand = new Random();
            switch (genderVariantsEnum) {
                case MALE:
                    names = namesOfResponse.stream().filter(name -> name.getGender() == GenderTypeEnum.MALE).collect(Collectors.toList());
                    break;
                case FEMALE:
                    names = namesOfResponse.stream().filter(name -> name.getGender() == GenderTypeEnum.FEMALE).collect(Collectors.toList());
                    break;
                default:
                case ANY:
                    names = new ArrayList<>(namesOfResponse);
                    break;
            }
            if (names.isEmpty()) {
                return Optional.empty();
            } else {
                final Name name = names.get(rand.nextInt(names.size()));
                return Optional.of(name);
            }
        } else {
            return Optional.empty();
        }
    }
}
