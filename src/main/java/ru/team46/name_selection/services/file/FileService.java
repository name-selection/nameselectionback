package ru.team46.name_selection.services.file;

import org.springframework.stereotype.Service;
import ru.team46.name_selection.entities.file.File;
import ru.team46.name_selection.repositories.file.FileRepository;
import ru.team46.name_selection.services.common.AbstractService;

import java.util.Optional;
import java.util.UUID;

@Service
public class FileService extends AbstractService<File, FileRepository> {
    public FileService(FileRepository repository) {
        super(repository);
    }

    @Override
    public Optional<File> delete(UUID uuid) {
        Optional<File> optionalFile = repository.findById(uuid);
        if (optionalFile.isPresent()) {
            repository.deleteById(uuid);
        }
        return optionalFile;
    }
}
