package ru.team46.name_selection.services.common;

import ru.team46.name_selection.entities.common.AbstractEntity;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface ICommonService<E extends AbstractEntity> {
    Optional<E> save(E entity);

    Optional<E> update(E entity);

    Optional<List<E>> findAll();

    Optional<E> findByUUID(UUID uuid);

    Optional<E> delete(UUID uuid);
}
