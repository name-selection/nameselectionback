package ru.team46.name_selection.services.common;
import org.springframework.beans.factory.annotation.Autowired;
import ru.team46.name_selection.entities.common.AbstractEntity;
import ru.team46.name_selection.repositories.common.ICommonRepository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

public abstract class AbstractService<E extends AbstractEntity,
        R extends ICommonRepository<E>> implements ICommonService<E> {
    protected final R repository;

    @Autowired
    public AbstractService(R repository) {
        this.repository = repository;
    }

    @Override
    public Optional<E> save(E entity) {
        return Optional.of(repository.save(entity)) ;
    }

    @Override
    public Optional<E> update(E entity) {
        return this.save(entity);
    }

    @Override
    public Optional<List<E>> findAll() {
        final List<E> entities = repository.findAll();
        final List<E> notDeletedEntities = entities.stream().filter(e -> !e.isDeleted()).collect(Collectors.toList());
        return entities.isEmpty() ? Optional.empty() : Optional.of(notDeletedEntities);
    }

    @Override
    public Optional<E> findByUUID(UUID uuid) {
        final Optional<E> entity = repository.findById(uuid);
        if (entity.isPresent() && !entity.get().isDeleted()) {
            return entity;
        } else {
            return Optional.empty();
        }
    }

    @Override
    public Optional<E> delete(UUID uuid) {
        final Optional<E> optionalE = this.findByUUID(uuid);
        if (optionalE.isPresent()) {
            final E entity = optionalE.get();
            entity.setDeleted(true);
            return this.save(entity);
        } else {
            return Optional.empty();
        }

    }
}
